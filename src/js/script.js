$(document).ready(() => {

  /**
   * Open / close nav
   */

  $('.s-burger').on('click', function () {
    if ($('.s-nav').hasClass('s-nav--is-visible')) {
      $('.s-nav').removeClass('s-nav--is-visible');
      $(this).removeClass('s-burger--close');
      $('body').removeClass('prevent-scroll');
    } else {
      $('.s-nav').addClass('s-nav--is-visible');
      $(this).addClass('s-burger--close');
      $('body').addClass('prevent-scroll');
    }
  });

  $('.s-nav__field').on('click', function () {
    $('.s-nav').removeClass('s-nav--is-visible');
    $(this).removeClass('s-burger--close');
    $('body').removeClass('prevent-scroll');
  });



  /**
   * Swiper sliders
   */

  var swiper = new Swiper('.s-featured-news__slider', {
    pagination: {
      el: '.s-featured-news__slider-progressbar',
      type: 'progressbar'
    },
    navigation: {
      prevEl: '.s-featured-news__slider-nav .s-slider-nav__btn--prev',
      nextEl: '.s-featured-news__slider-nav .s-slider-nav__btn--next'
    },
    breakpoints: {
      320: {
        slidesPerView: 1,
        spaceBetween: 20
      },
      576: {
        slidesPerView: 2,
        spaceBetween: 20
      },
      768: {
        slidesPerView: 2,
        spaceBetween: 40
      },
      1200: {
        slidesPerView: 'auto',
        spaceBetween: 40,
        freeMode: {
          enabled: true,
          sticky: true
        }
      }
    }
  });

  var swiper = new Swiper('.s-featured-products__slider', {
    pagination: {
      el: '.s-featured-products__slider-progressbar',
      type: 'progressbar'
    },
    navigation: {
      prevEl: '.s-featured-products__slider-nav .s-slider-nav__btn--prev',
      nextEl: '.s-featured-products__slider-nav .s-slider-nav__btn--next'
    },
    breakpoints: {
      320: {
        slidesPerView: 1,
        spaceBetween: 20
      },
      576: {
        slidesPerView: 2,
        spaceBetween: 20
      },
      768: {
        slidesPerView: 3,
        spaceBetween: 40
      },
      1200: {
        slidesPerView: 4,
        spaceBetween: 40
      }
    }
  });

  var swiper = new Swiper('.s-team__slider', {
    pagination: {
      el: '.s-team__slider-progressbar',
      type: 'progressbar'
    },
    navigation: {
      prevEl: '.s-team__slider-nav .s-slider-nav__btn--prev',
      nextEl: '.s-team__slider-nav .s-slider-nav__btn--next'
    },
    breakpoints: {
      320: {
        slidesPerView: 1,
        spaceBetween: 20
      },
      576: {
        slidesPerView: 2,
        spaceBetween: 20
      },
      768: {
        slidesPerView: 2,
        spaceBetween: 40
      },
      992: {
        slidesPerView: 3,
        spaceBetween: 40
      }
    }
  });

  var swiper = new Swiper('.s-service__slider', {
    pagination: {
      el: '.s-service__slider-progressbar',
      type: 'progressbar'
    },
    navigation: {
      prevEl: '.s-service__slider-nav-btn--prev',
      nextEl: '.s-service__slider-nav-btn--next'
    }
  });



  /**
   * Turn on tabs
   */

  var tabSelectors = document.querySelectorAll('[data-tabs]');
  if (tabSelectors) {
    for (let [i, tabs] of [...tabSelectors].entries()) {
      tabs.setAttribute('data-tabs', i);
      new Tabby(`[data-tabs="${i}"]`);
    }
  }
});